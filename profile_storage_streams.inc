<?php

/**
 * @file
 * Provides a profile default files (profile://) stream wrapper class.
 */
class ProfileStorageStreamWrapper extends DrupalPublicStreamWrapper {
  public function getDirectoryPath() {
    return 'sites/all/storage';
  }
  
  /**
   * Overrides getExternalUrl().
   *
   * Note: duplicate method to avoid using parent class 'getDirectoryPath()'
   * call that returns generic 'public://' directory path.
   *
   * Return the HTML URI of a public file.
   */
  function getExternalUrl() {
    $path = str_replace('\\', '/', $this->getTarget());
    return $GLOBALS['base_url'] . '/' . self::getDirectoryPath() . '/' . drupal_encode_path($path);
  }
}
